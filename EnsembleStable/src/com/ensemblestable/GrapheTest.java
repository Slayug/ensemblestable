package com.ensemblestable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GrapheTest {

	public static void main(String[] args) {
		//TEST
		Graphe grapheNoConnected = new Graphe();
		Map<Integer, List<Integer>> vertex = new HashMap<Integer, List<Integer>>();;
		List<Integer> zero = new ArrayList<Integer>();
		zero.add(1);
		zero.add(2);
		vertex.put(0, zero);
		
		List<Integer> one = new ArrayList<Integer>();
		one.add(0);
		one.add(2);
		vertex.put(1, one);
		
		List<Integer> two = new ArrayList<Integer>();
		two.add(0);
		two.add(1);
		vertex.put(2, two);
		
		List<Integer> three = new ArrayList<Integer>();
		three.add( 4 );
		vertex.put(3, three);
		
		List<Integer> four = new ArrayList<Integer>();
		four.add( 3 );
		vertex.put(4, four);
		
		List<Integer> five = new ArrayList<Integer>();
		five.add(6);
		five.add(7);
		vertex.put(5, five);
		List<Integer> six = new ArrayList<Integer>();
		six.add(5);
		six.add(8);
		vertex.put(6, six);
		List<Integer> seven = new ArrayList<Integer>();
		seven.add(5);
		seven.add(9);
		vertex.put(7, seven);
		List<Integer> eight = new ArrayList<Integer>();
		eight.add(6);
		vertex.put(8, eight);
		List<Integer> nine = new ArrayList<Integer>();
		nine.add(7);
		vertex.put(9, nine);
		
		grapheNoConnected.setVertex( vertex );
		
		System.out.println( grapheNoConnected.toString() );
		
		System.out.println("======== IS CONNECTED ========");
		System.out.println("grapheNoConnected: " + grapheNoConnected.isConnected() );
		
		List<Integer> list1 = zero;
		List<Integer> list2 = one;
		System.out.println("======== IS INCLUDE ========");
		System.out.println("grapheNoConnected: " + grapheNoConnected.isInclude(list1,list2));

		System.out.println("======== getOneNoConnected ========");
		System.out.println("getOneNoConnected: " + grapheNoConnected.getOneNoConnected());
		
		System.out.println("======== do2Fold ========");
		System.out.println("getOneNoConnected: " + grapheNoConnected.do2Fold(5));
		
		List<Integer> sous_graphe = new ArrayList<Integer>();
		sous_graphe.add(0);
		sous_graphe.add(1);
		sous_graphe.add(2);
		
		List<Integer> sous_graphe2 = new ArrayList<Integer>();
                sous_graphe2.add(5);
                sous_graphe2.add(6);
                sous_graphe2.add(7);
                sous_graphe2.add(8);
		
                System.out.println("======== IS CLICKABLE ========");
		System.out.println("sous graphe 1 : " + grapheNoConnected.isClickable(sous_graphe));
		System.out.println("sous graphe 2 : " + grapheNoConnected.isClickable(sous_graphe2));
		
		System.out.println("======== REMOVE VERTEX ========");
		Graphe g = grapheNoConnected.removeVertex(0);
		System.out.println(g.toString());
	}
}
