package com.ensemblestable;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Parser {

    public static Graphe parser(String path) throws Exception {
        Graphe g = new Graphe();
        Scanner sc = new Scanner(new FileInputStream(path), "UTF-8");
        List<Integer> vertex = new ArrayList<Integer>();
        
        String str;
        int line = 0, i, sommet = 0, nbSommet = 0;
        
        while (sc.hasNext()) {
            str = sc.nextLine();
            str = str.replaceAll(" ", "");
            
            if (line == 0) {
                nbSommet = Integer.parseInt(str);
            } 
            else {
                i = Integer.parseInt(str.substring(0, 1));
                
                if (!(sommet == i)) {
                    sc.close();

                    throw new Exception("Error at line " + line + ":\nPas dans l'odre");
                } else if (!(i < nbSommet)) {
                    sc.close();

                    throw new Exception("Error at line " + line + ":\nL'indice de sommet est supérieur à celui saisi au début du fichier");
                }
                
                try {
                    vertex = createVertex(str.substring(2));
                    g.getVertex().put(i, vertex);
                    
                    sommet++;
                } catch (Exception e) {
                    sc.close();
                    
                    throw new Exception("Error at line " + line + ":\n" + e.getMessage());
                }
            }
            
            line++;
        }
        
        sc.close();
        
        return g;
    }
    
    public static List<Integer> createVertex(String str) throws Exception {
        List<Integer> vertex = new ArrayList<Integer>();
        StringTokenizer st;
        int count, value;
        
        if (!(str.contains("[") && str.contains("]"))) throw new Exception("Erreur à la saisi de la liste");
        
        str = str.substring(1, str.length() - 1);
        value = str.replaceAll(",", "").length();
        count = str.length() - value;
        
        if (count == value) return vertex;
        
        st = new StringTokenizer(str, ",");
        
        while (st.hasMoreTokens()) {
            vertex.add(Integer.parseInt(st.nextToken()));
        }
        
        return vertex;
    }
}
