package com.ensemblestable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Iterator;

public class Graphe {

	private Map<Integer, List<Integer> > vertex = new HashMap<Integer, List<Integer> >();


	public Map<Integer, List<Integer>> getVertex() {
		return vertex;
	}

	public void setVertex(Map<Integer, List<Integer>> vertex) {
		this.vertex = vertex;
	}

	public boolean isConnected() {
		return isConnected(this);
	}

	public List<Integer> neighboursOf(int idVertex, int distance) {
		return neighboursOf(this, idVertex, distance);
	}

	public List<Integer> mirror(int idVertex) {
		return mirror(this, idVertex);
	}

	public int numberVertex() {
		return numberVertex(this);
	}

	public boolean is2Foldable(int idVertex) {
		return is2Foldable(idVertex);
	}

	public Graphe do2Fold(int idVertex){
		return do2Fold(this,idVertex);
	}

	public Graphe getOneNoConnected() {
		return getOneNoConnected(this);
	}

	public boolean isClickable(List<Integer> list) {
		return isClickable(this, list);
	}

	public Graphe removeVertex(int idVertex) {
		return removeVertex(this, idVertex);
	}

	public boolean isInclude(List<Integer> list1,List<Integer> list2){
		return isInclude(this,list1,list2);
	}

	public static boolean isConnected(Graphe g) {
		//on fait un parcours en profondeur,
		//si on atteint tous les sommets, 
		//alors connexe (connected)
		//sinon non connexe

		Set<Integer> keyVertex = g.getVertex().keySet();
		int firstIndex = (int) keyVertex.toArray()[ 0 ];
		List<Integer> subGraph = depthExploring(g, firstIndex);
		if( subGraph.size() == g.getVertex().size() ){
			return true;
		}
		return false;
	}

	/**
	 * Parcours le graphe g depuis le sommet vertex
	 * @param g
	 * @param vertex
	 * @return Retourne la liste des sommet(s) atteint(s) depuis le sommet donné (vertex)
	 */
	public static List<Integer> depthExploring(Graphe g, int vertex){

		List<Integer> vertexFlagged = new ArrayList<Integer>();
		vertexFlagged.add( vertex );

		//liste des sommets à parcrourir
		//voisins des sommets découverts
		List<Integer> heapVertex = new ArrayList<Integer>();
		heapVertex.add( vertex );
		while( heapVertex.size() > 0 ){
			List<Integer> neighbours = g.neighboursOf( heapVertex.get( 0 ), 1);
			if( neighbours != null ){
				for(int n = 0; n < neighbours.size(); n++){
					if( !vertexFlagged.contains( neighbours.get( n ) ) ){
						vertexFlagged.add( neighbours.get( n ) );
						heapVertex.add( neighbours.get( n ) );
					}
				}
			}
			heapVertex.remove( 0 );
		}
		return vertexFlagged;
	}

	public static List<Integer> neighboursOf(Graphe g, int idVertex,
			int distance) {
		if( distance == 1 ){
			return g.getVertex().get( idVertex );
		}else if( distance == 2 ){
			List<Integer> neighbours1 = g.getVertex().get( idVertex );
			List<Integer> neighbours2 = new ArrayList<Integer>();
			for( int n = 0; n < neighbours1.size(); n++){
				neighbours2.addAll( g.getVertex().get( neighbours1.get( n ) ) );
			}
			return neighbours2;
		}else{
			System.err.println("Distance doit être > 0 && < 3");
			return null;
		}
	}

	/**
	 * Retourne la liste des miroirs du sommet idVertex
	 * Un sommet s est un miroir si il est � distance 2 de idVertex et 
	 * si les voisins de idVertex sans les voisins de s forme une clique
	 * @param g idVertex
	 * @return Retourne la liste correspondante
	 */
	public static List<Integer> mirror(Graphe g, int idVertex) {
		Graphe gClone = g.clone();
		List<Integer> listeMiroir = new ArrayList<Integer>();
		List<Integer> voisins2 = gClone.neighboursOf(idVertex, 2);

		List<Integer> voisins1;
		List<Integer> voisinstmp;
		Iterator<Integer> it = voisins2.iterator();
		while( it.hasNext() ){
			Integer tmp = it.next();
			voisins1 = gClone.neighboursOf(idVertex, 1);
			voisinstmp = gClone.neighboursOf(tmp, 1);
			voisins1.removeAll(voisinstmp);
			if(g.isClickable(voisins1))
				listeMiroir.add(tmp);
		}

		return listeMiroir;
	}
	
	public int getVertexWithMaxDegree(){
		Map<Integer, List<Integer>> vertex = this.getVertex();
		Set<Integer> keyVertex = vertex.keySet();
		int indexVertex = 0;
		int maxNeigh = 0;
		for(Integer index : keyVertex){
			if( vertex.get( index ).size() == 2 ){
				int localMaxNeigh = 0;
				for(int v = 0; v < vertex.get( index ).size(); v++){
					localMaxNeigh += vertex.get( vertex.get(index).get( v ) ).size();
				}
				if( localMaxNeigh > maxNeigh){
					maxNeigh = localMaxNeigh;
					indexVertex = index;
				}
			}
		}
		return indexVertex;
	}

	/**
	 * Retourne le nombre de sommets du graphe
	 * @param g
	 * @return Retourne le boolean correspondant
	 */
	public static int numberVertex(Graphe g) {
		return g.getVertex().keySet().size();
	}

	/**
	 * Verifie si le sommet idVertex est 2pliable (exactement 2 voisins qui eux mêmes ne sont pas voisins)
	 * @param g
	 * @param idVertex
	 * @return Retourne le boolean correspondant
	 */
	public static boolean is2Foldable(Graphe g, int idVertex) {
		if(g.getVertex().get(idVertex).size() == 2){
			Iterator<Integer> it = g.getVertex().get(idVertex).iterator();
			Integer u1 = it.next();
			Integer u2 = it.next();
			return !g.getVertex().get(u1).contains(u2);
		}
		return false;
	}

	/**
	 * On fait le 2pliage en supprimant le sommet concerné, ses 2 voisins et en créant
	 * un nouveau sommet ayant pour voisins les voisins des deux voisins (son nom final
	 * est le sommet en parametre, on se contente de le reprendre)
	 * on ajoute aussi le sommet dans les listes des voisins
	 * @param g
	 * @param idVertex
	 * @return Un Graphe
	 */
	public static Graphe do2Fold(Graphe g,int idVertex){
		if(!is2Foldable(g,idVertex))
			return g;
		Iterator<Integer> it = g.getVertex().get(idVertex).iterator();
		Integer u1 = it.next();
		Integer u2 = it.next();
		List<Integer> voisins1 = neighboursOf(g, u1, 1);
		List<Integer> voisins2 = neighboursOf(g, u2, 1);
		voisins1.remove((Integer)idVertex);
		voisins2.remove((Integer)idVertex);
		for(Integer i : voisins2){
			if(!voisins1.contains(i))
				voisins1.add(i);
		}
		g = removeVertex(g, idVertex);
		g = removeVertex(g, u1);
		g = removeVertex(g, u2);
		for(Integer i : voisins1){
			g.getVertex().get(i).add(idVertex);
		}
		g.getVertex().put(idVertex, voisins1);
		return g;
	}

	/**
	 * Si le graphe est connexe, retourne le graphe en entier,
	 * Sinon par du dernier sommet et retourne tous les sommets
	 * atteints depuis celui-ci
	 * @param Graphe à traiter
	 * @return Un Graphe
	 */
	public static Graphe getOneNoConnected(Graphe g) {
		List<Integer> listVertex = new ArrayList<Integer>();
		listVertex.addAll( g.getVertex().keySet() );
		List<Integer> vertexFromExploring = depthExploring( g, listVertex.get( listVertex.size() - 1 ) );
		Graphe newGraphe = new Graphe();
		Map<Integer, List<Integer>> vertex = new HashMap<Integer, List<Integer>>();
		Graphe grapheClone = g.clone();
		for(int v = 0; v < vertexFromExploring.size(); v++){
			List<Integer> vert = grapheClone.vertex.get( vertexFromExploring.get( v ) );
			vertex.put(vertexFromExploring.get( v ), vert);
		}
		newGraphe.setVertex( vertex );

		return newGraphe;
	}


	/**
	 * Teste si le sous-graphe est une clique
	 * @param g le graphe G
	 * @param list le sous-graphe H
	 * @return vrai si le sous-graphe est une clique, faux sinon
	 */
	public static boolean isClickable(Graphe g, List<Integer> list) {
		if (list.size() == 0) return true;

		// création d'un sous-graphe h à partir de list
		Graphe h = new Graphe();

		list.forEach(e -> {
			List<Integer> list2 = g.vertex.get(e).stream()
					.filter(i -> list.contains(i))
					.collect(Collectors.toList());

			list2.remove((Integer) e);
			h.vertex.put(e, list2);
		});

		for (Entry<Integer, List<Integer> > entry : h.vertex.entrySet()) {
			List<Integer> l = new ArrayList<Integer>(entry.getValue());
			l.add(entry.getKey());

			if (!(l.containsAll(list) && l.size() == list.size())) {
				return false;
			}
		}

		return true;
	}

	public static Graphe removeVertex(Graphe g, int idVertex) {
		Graphe g2 = g.clone();
		g2.vertex.remove(idVertex);

		g2.vertex.forEach((key, value) -> {
			value.remove((Integer) idVertex);
		});

		return g2;
	}
	

	public Graphe removeVertexFromGraphe(Graphe g){

		List<Integer> li = new ArrayList<Integer>();
		li.addAll( g.getVertex().keySet() );
		return this.removeVertex( li );
	}



	public Graphe removeVertex(List<Integer> vertex){
		Graphe clone = this.clone();

		for(int v = 0; v < vertex.size(); v++){
			int idVertex = vertex.get( v );
			clone.vertex.remove(idVertex);
			clone.vertex.forEach((key, value) -> {
				value.remove((Integer) idVertex);
			});
		}
		return clone;
	}

	/**
	 * Retourne un sommet v qui est de degré deux,
	 * si aucun sommet est de degré deux, alors retourne
	 * -1
	 * @return
	 */
	public int containsOneVertexTwoDegreeAndNeighboursNotConnected(){
		Map<Integer, List<Integer>> vertex = this.getVertex();
		Set<Integer> keyVertex = vertex.keySet();
		for(Integer index : keyVertex){
			if( vertex.get( index ).size() == 2 &&
					! vertex.get( vertex.get( index).get( 0 )).contains( vertex.get( vertex.get( index ) ).get( 1 ) ) ){
				//Si le sommet est de degré deux et que ses deux voisins ne soient pas 
				//connectés entre eux.
				return index;
			}
		}
		return -1;
	}

	/**
	 * Verifie si une liste de sommets est incluse dans une autre
	 * @param list
	 * @param list2
	 * @return Retourne le boolean correspondant
	 */
	public static boolean isInclude(Graphe g,List<Integer> list, List<Integer> list2) {
		Iterator<Integer> it=list.iterator();
		while(it.hasNext()){
			Integer i=(Integer)it.next();
			if(!list2.contains(i))
				return false;
		}
		return true;
	}

	/**
	 * Si il existe deux sommets v et w tels que les voisins de w
	 * sont inclus dans les voisins de v
	 * @return Retourne le sommet v (sous forme d'index) si c'est vrai, Sinon retourne -1
	 */
	public int containsOneVertexNeighboursOfOtherOne(){
		//on parcourt tous les sommets que l'on test à chaque voisins de ce sommet
		//vérifiant si les voisins du sommet parcouru sont contenus dans les voisins d'un sommet voisin
		Map<Integer, List<Integer>> vertex = this.getVertex();
		Set<Integer> keyVertex = vertex.keySet();
		for(Integer index : keyVertex){
			List<Integer> currentVertex = this.getNeighboursWithHimSelf( index );
			for(int c = 0; c < currentVertex.size(); c++){
				//parcours des voisins
				List<Integer> neighboursOfNeighbours = this.getNeighboursWithHimSelf( currentVertex.get( c ));
				
				if( neighboursOfNeighbours.containsAll( currentVertex ) ){
					return currentVertex.get( c );
				}
			}
		}
		return -1;
	}

	public List<Integer> getNeighboursWithHimSelf(int vertex){
		List<Integer> neigh = new ArrayList<Integer>();
		for(int n = 0; n < this.getVertex().get( vertex ).size(); n++){
			neigh.add( this.getVertex().get( vertex).get( n ) );
		}
		neigh.add( vertex );
		return neigh;
	}
	
	@Override
	public Graphe clone() {
		Graphe g = new Graphe();

		this.vertex.forEach((key, value) -> {
			List<Integer> list = new ArrayList<Integer>(value);
			g.vertex.put(key, list);
		});
		return g;
	}



	@Override
	public String toString() {
		return "Graphe [vertex=" + vertex + "]";
	}



}
