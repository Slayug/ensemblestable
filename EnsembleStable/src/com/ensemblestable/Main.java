package com.ensemblestable;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println("Erreur : Aucun argument à l'entrée");
        } else {
            try {
                Graphe g = Parser.parser(args[0]);
                System.out.println(g);

                System.out.println(mis(g));
                System.out.println("FINI");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static int mis(Graphe g) {
        int neigh = 0;
        if (g.getVertex().size() <= 1) {
            return g.getVertex().size();
        } else if (!g.isConnected()) {
            Graphe c = g.getOneNoConnected();

            Graphe graphWithoutC = g.removeVertexFromGraphe(c);
            return mis(c) + mis(graphWithoutC);
        } else if ((neigh = g.containsOneVertexNeighboursOfOtherOne()) >= 0) {
            // si il existe deux sommets v et w tels que les voisins de w sont
            // inclus dans les voisins de v
            Graphe nG = g.removeVertex(neigh);
            // System.out.println( nG );
            return mis(nG);
        } else if ((neigh = g.containsOneVertexTwoDegreeAndNeighboursNotConnected()) >= 0) {
            // il existe un sommet v de degré 2 et qui est deux pliable
            return 1 + mis(g.do2Fold(neigh));
        } else {
            // Soit v un sommet de degré maximum, dont le nombre
            // d'arrêtes entre deux sommets de N(v) est maxiumum
            neigh = g.getVertexWithMaxDegree(); // soit neigh = v
            List<Integer> mirrorOfV = g.mirror(neigh);

            List<Integer> neighboursV = g.getVertex().get(neigh);
            // System.out.println("voisin de V");
            Graphe gClean = g.removeVertex(mirrorOfV);
            gClean = gClean.removeVertex(neigh);

            return Math.max(mis(gClean), 1 + mis(g.removeVertex(neighboursV)));
        }

    }
}
